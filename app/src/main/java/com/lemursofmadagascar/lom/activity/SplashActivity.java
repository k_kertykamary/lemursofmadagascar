package com.lemursofmadagascar.lom.activity;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.lemursofmadagascar.lom.R;
import com.lemursofmadagascar.lom.model.database.Author;
import com.lemursofmadagascar.lom.model.database.Family;
import com.lemursofmadagascar.lom.model.database.Illustration;
import com.lemursofmadagascar.lom.model.database.LemurWatchingSite;
import com.lemursofmadagascar.lom.model.database.Link;
import com.lemursofmadagascar.lom.model.database.Map;
import com.lemursofmadagascar.lom.model.database.Menu;
import com.lemursofmadagascar.lom.model.database.Photograph;
import com.lemursofmadagascar.lom.model.database.Species;

import java.io.IOException;
import java.io.InputStream;

public class SplashActivity extends BaseActivity {

    private TextView textViewVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_splash);
        this.hideActionBar();
        waitUntilAndStartActivity(3, MainActivity.class);
    }

    @Override
    protected void initViews() {
        textViewVersion = (TextView) findViewById(R.id.textViewVersion);
    }

    @Override
    protected void setViewsValuesAndListeners() {
        if (textViewVersion != null){
            try {
                textViewVersion.setText(String.format("Version %s", getPackageManager().getPackageInfo(getPackageName(), 0).versionName));
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                textViewVersion.setText("");
            }
        }
    }

    
    private void waitUntilAndStartActivity(int delay, final Class destinationActivity){
        int delayInMillis = delay * 1000;
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                launchActivity(destinationActivity, null, true);
            }
        }, delayInMillis);
    }
}
