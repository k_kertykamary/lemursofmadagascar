package com.lemursofmadagascar.lom.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.lemursofmadagascar.lom.R;
import com.lemursofmadagascar.lom.model.database.Author;
import com.lemursofmadagascar.lom.tools.GenericTools;
import com.nostra13.universalimageloader.core.ImageLoader;

public class AuthorDetailsActivity extends BaseActivity {

    public static final String BUNDLE_KEY_AUTHOR = "bundle_key_author";
    private Author author;

    private ImageView imageViewAuthor;
    private TextView textViewAuthorContent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ico_back);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        /*
        * Get extras params
        * */
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.author = extras.getParcelable(BUNDLE_KEY_AUTHOR);
        }

        /*
        * Set toolbar title
        * */
        if (this.author != null) {
            if (!GenericTools.isNullOrEmpty(this.author.getName())) {
                setTitle(this.author.getName());
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void setTitle(CharSequence title) {
        ((TextView)findViewById(R.id.toolbarTitle)).setText(title);
    }

    @Override
    protected void initViews() {
        this.imageViewAuthor = (ImageView) findViewById(R.id.imageViewAuthor);
        this.textViewAuthorContent = (TextView) findViewById(R.id.textViewAuthorContent);
    }

    @Override
    protected void setViewsValuesAndListeners() {
        if (this.author != null){

            if (!GenericTools.isNullOrEmpty(this.author.getPhoto())){
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.displayImage(author.getPhoto(), this.imageViewAuthor);
            }else{
                Log.d(AuthorDetailsActivity.class.getCanonicalName(), "Author ID : " + this.author.id + "doesn't have photo ....");
            }

            if (!GenericTools.isNullOrEmpty(this.author.getDetails())){
                this.textViewAuthorContent.setText(this.author.getDetails());
            }else{
                Log.d(AuthorDetailsActivity.class.getCanonicalName(), "Author ID : " + this.author.id + "doesn't have details ....");
            }
        }
    }
}
