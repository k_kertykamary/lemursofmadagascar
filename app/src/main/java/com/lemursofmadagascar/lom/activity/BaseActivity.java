package com.lemursofmadagascar.lom.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.lemursofmadagascar.lom.dialog.ProgressDialog;

/**
 * Created by Kerty on 06/01/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    protected abstract void initViews();
    protected abstract void setViewsValuesAndListeners();


    @Override
    protected void onStart() {
        super.onStart();
        this.initViews();
        this.setViewsValuesAndListeners();
    }

    /**
     *
     * @return true if internet connexion is available
     */
    protected boolean isNetworkAvailable()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Hide the ActionBar
     */
    protected void hideActionBar(){
        if (getSupportActionBar() != null){
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            getSupportActionBar().hide();
        }
    }

    /**
     * Show loader
     */
    protected void showProgressDialog(){
        if (progressDialog == null){
            progressDialog = new ProgressDialog(this);
        }
        progressDialog.show();
    }

    /**
     * Hide/dismiss the loader
     */
    protected void dismissProgressDialog(){
        if (progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


    /**
     * Launch an activity
     * @param destinationActivity
     * @param params Parameters
     * @param needToFinish set true if need to finish the activity sender.
     */
    public void launchActivity(Class destinationActivity, @Nullable Bundle params, boolean needToFinish){
        Intent intent = new Intent(this, destinationActivity);
        if (params != null){
            intent.putExtras(params);
        }
        startActivity(intent);
        if (needToFinish){
            finish();
        }
    }

}
