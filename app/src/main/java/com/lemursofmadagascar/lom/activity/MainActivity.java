package com.lemursofmadagascar.lom.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.lemursofmadagascar.lom.R;
import com.lemursofmadagascar.lom.fragment.AboutTheAuthorsFragment;
import com.lemursofmadagascar.lom.fragment.ExtinctLemursFragment;
import com.lemursofmadagascar.lom.fragment.FamiliesFragment;
import com.lemursofmadagascar.lom.fragment.IntroductionFragment;
import com.lemursofmadagascar.lom.fragment.OriginOfLemursFragment;
import com.lemursofmadagascar.lom.fragment.SpeciesFragment;
import com.lemursofmadagascar.lom.fragment.WatchingSitesFragment;
import com.lemursofmadagascar.lom.fragment.listener.IBaseFragmentListener;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, IBaseFragmentListener, View.OnClickListener {

    private Toolbar toolbar;
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        if (toolbar != null) {
            toolbar.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.ico_menu));
        }
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
        }

        if (navigationView != null) {
            navigationView.setCheckedItem(R.id.nav_intro);
            onNavigationItemSelected(navigationView.getMenu().findItem(R.id.nav_intro));
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.nav_intro:
                pushFragment(IntroductionFragment.class, null);
                break;
            case R.id.nav_origin:
                pushFragment(OriginOfLemursFragment.class, null);
                break;
            case R.id.nav_extinct:
                pushFragment(ExtinctLemursFragment.class, null);
                break;
            case R.id.nav_about_author:
                pushFragment(AboutTheAuthorsFragment.class, null);
                break;
            case R.id.nav_species:
                pushFragment(SpeciesFragment.class, null);
                break;
            case R.id.nav_families:
                pushFragment(FamiliesFragment.class, null);
                break;
            case R.id.nav_watching_sites:
                pushFragment(WatchingSitesFragment.class, null);
                break;
            case R.id.nav_sightings:
                break;
            case R.id.nav_primate_watching:
                break;
            case R.id.nav_about_app:
                break;
            default:
                break;
        }

        setTitle(item.getTitle());
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void setTitle(CharSequence title) {
        ((TextView)findViewById(R.id.toolbarTitle)).setText(title);
    }

    @Override
    protected void initViews() {/* no need to implements */}

    @Override
    protected void setViewsValuesAndListeners() {
        /* no need to implements */
    }

    @Override
    public void hideSearchMenu(boolean hide) {
        if (!hide){
            findViewById(R.id.toolbar_ico_right).setVisibility(View.INVISIBLE);
            findViewById(R.id.toolbar_ico_right).setOnClickListener(null);
        }else {
            findViewById(R.id.toolbar_ico_right).setVisibility(View.VISIBLE);
            findViewById(R.id.toolbar_ico_right).setOnClickListener(this);
        }
    }

    private void pushFragment(Class fragmentClass, Bundle bundle){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_fragment, Fragment.instantiate(this, fragmentClass.getName(), bundle )).commit();
    }


    @Override
    public void onClick(View view) {

    }
}
