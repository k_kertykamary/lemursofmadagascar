package com.lemursofmadagascar.lom.model.database;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.lemursofmadagascar.lom.tools.GenericTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Photograph extends RealmObject implements Parcelable {

    @PrimaryKey
    private long id;

    private String photograph;
    private String title;

    public Photograph(JSONObject jsonObject) {

        try {
            if (jsonObject.has("_nid") && !jsonObject.isNull("_nid")){
                long jsonId = Long.parseLong(jsonObject.getString("_nid"));
                if (Photograph.getById(jsonId) == null){
                    this.id = jsonId;
                }
                if (jsonObject.has("_title") && !jsonObject.isNull("_title")){
                    this.title = jsonObject.getString("_title");
                }else {
                    this.title = "";
                }
                if (jsonObject.has("_photograph") && !jsonObject.isNull("_photograph")){
                    this.photograph = jsonObject.getString("_photograph");
                }else {
                    this.photograph = "";
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static Menu getById(long id) {
        Realm realm = Realm.getDefaultInstance();
        return  realm.where(Menu.class).equalTo("id",id).findFirst();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhotograph() {
        return photograph;
    }

    public void setPhotograph(String photograph) {
        this.photograph = photograph;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.photograph);
        dest.writeString(this.title);
    }

    protected Photograph(Parcel in) {
        this.id = in.readLong();
        this.photograph = in.readString();
        this.title = in.readString();
    }

    public static final Creator<Photograph> CREATOR = new Creator<Photograph>() {
        @Override
        public Photograph createFromParcel(Parcel source) {
            return new Photograph(source);
        }

        @Override
        public Photograph[] newArray(int size) {
            return new Photograph[size];
        }
    };

    public static void initData(String data) {
        if (!GenericTools.isNullOrEmpty(data)){
            try {
                JSONObject jsonObject = new JSONObject(data);
                final JSONArray jsonArray = jsonObject.getJSONArray("photographs");

                Realm realm = Realm.getDefaultInstance();
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    try {
                        realm.beginTransaction();
                        Photograph photograph = new Photograph(jsonArray.getJSONObject(i));
                        realm.copyToRealmOrUpdate(photograph);
                        realm.commitTransaction();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Log.d(Photograph.class.getName(), "No data json ....");
        }
    }

    public Photograph() {
    }

    public String getPhoto(){
        if (this.title != null && !this.title.isEmpty()){
            return String.format("assets://photos/%s.jpg", this.photograph);
        }else {
            return null;
        }
    }
}
