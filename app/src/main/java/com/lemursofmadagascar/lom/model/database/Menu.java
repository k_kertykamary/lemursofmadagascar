package com.lemursofmadagascar.lom.model.database;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.lemursofmadagascar.lom.tools.GenericTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;


public class Menu extends RealmObject implements Parcelable {

    @PrimaryKey
    private long id;

    private String menuName;
    private String menuContent;

    public Menu(JSONObject jsonObject) {
        try {
            if (jsonObject.has("_nid") && !jsonObject.isNull("_nid")){
                long jsonId = Long.parseLong(jsonObject.getString("_nid"));
                if (Menu.getById(jsonId) == null){
                    this.id = jsonId;
                }
                if (jsonObject.has("_menu_name") && !jsonObject.isNull("_menu_name")){
                    this.menuName = jsonObject.getString("_menu_name");
                }else {
                    this.menuName = "";
                }
                if (jsonObject.has("_menu_content") && !jsonObject.isNull("_menu_content")){
                    this.menuContent = jsonObject.getString("_menu_content");
                }else {
                    this.menuContent = "";
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static Menu getById(long id) {
        Realm realm = Realm.getDefaultInstance();
        return  realm.where(Menu.class).equalTo("id",id).findFirst();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuContent() {
        return menuContent;
    }

    public void setMenuContent(String menuContent) {
        this.menuContent = menuContent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.menuName);
        dest.writeString(this.menuContent);
    }

    protected Menu(Parcel in) {
        this.id = in.readLong();
        this.menuName = in.readString();
        this.menuContent = in.readString();
    }

    public static final Creator<Menu> CREATOR = new Creator<Menu>() {
        @Override
        public Menu createFromParcel(Parcel source) {
            return new Menu(source);
        }

        @Override
        public Menu[] newArray(int size) {
            return new Menu[size];
        }
    };

    public static void initData(String data) {
        if (!GenericTools.isNullOrEmpty(data)){
            try {
                JSONObject jsonObject = new JSONObject(data);
                final JSONArray jsonArray = jsonObject.getJSONArray("menus");

                Realm realm = Realm.getDefaultInstance();

                for (int i = 0; i < jsonArray.length(); i++)
                {
                    try {
                        realm.beginTransaction();
                        Menu menu = new Menu(jsonArray.getJSONObject(i));
                        realm.copyToRealmOrUpdate(menu);
                        realm.commitTransaction();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Log.d(Menu.class.getName(), "No data json ....");
        }
    }


    public Menu() {
    }

    public static Menu getIntroductionContent() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Menu.class).equalTo("menuName", "introduction").findFirst();
    }

    public static Menu getOriginOfLemursContent() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Menu.class).equalTo("menuName", "originoflemurs").findFirst();
    }
}
