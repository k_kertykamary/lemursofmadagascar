package com.lemursofmadagascar.lom.model.database;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.lemursofmadagascar.lom.tools.GenericTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Illustration extends RealmObject implements Parcelable {

    @PrimaryKey
    private long id;

    private String illustration;
    private String illustrationDescription;

    public Illustration(JSONObject jsonObject) {
        try {
            if (jsonObject.has("_nid") && !jsonObject.isNull("_nid")){
                long jsonId = Long.parseLong(jsonObject.getString("_nid"));
                if (Illustration.getById(jsonId) == null){
                    this.id = jsonId;
                }
                if (jsonObject.has("_illustration") && !jsonObject.isNull("_illustration")){
                    this.illustration = jsonObject.getString("_illustration");
                }else {
                    this.illustration = "";
                }
                if (jsonObject.has("_illustration_description") && !jsonObject.isNull("_illustration_description")){
                    this.illustrationDescription = jsonObject.getString("_illustration_description");
                }else {
                    this.illustrationDescription = "";
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static Illustration getById(long id){
        Realm realm = Realm.getDefaultInstance();
        return  realm.where(Illustration.class).equalTo("id",id).findFirst();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIllustration() {
        return illustration;
    }

    public void setIllustration(String illustration) {
        this.illustration = illustration;
    }

    public String getIllustrationDescription() {
        return illustrationDescription;
    }

    public void setIllustrationDescription(String illustrationDescription) {
        this.illustrationDescription = illustrationDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.illustration);
        dest.writeString(this.illustrationDescription);
    }

    protected Illustration(Parcel in) {
        this.id = in.readLong();
        this.illustration = in.readString();
        this.illustrationDescription = in.readString();
    }

    public static final Creator<Illustration> CREATOR = new Creator<Illustration>() {
        @Override
        public Illustration createFromParcel(Parcel source) {
            return new Illustration(source);
        }

        @Override
        public Illustration[] newArray(int size) {
            return new Illustration[size];
        }
    };

    public static void initData(String data) {
        if (!GenericTools.isNullOrEmpty(data)){
            try {
                JSONObject jsonObject = new JSONObject(data);
                final JSONArray jsonArray = jsonObject.getJSONArray("illustrations");

                final Realm realm = Realm.getDefaultInstance();

                for (int i = 0; i < jsonArray.length(); i++)
                {
                    try {
                        realm.beginTransaction();
                        Illustration illustration = new Illustration(jsonArray.getJSONObject(i));
                        realm.copyToRealmOrUpdate(illustration);
                        realm.commitTransaction();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Log.d(Illustration.class.getName(), "No data json ....");
        }
    }

    public Illustration() {
    }
}
