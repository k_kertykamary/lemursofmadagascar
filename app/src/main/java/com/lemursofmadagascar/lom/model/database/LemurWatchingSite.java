package com.lemursofmadagascar.lom.model.database;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.lemursofmadagascar.lom.tools.GenericTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;


public class LemurWatchingSite extends RealmObject implements Parcelable {

    @PrimaryKey
    private long id;

    private String title;
    private String body;

    public LemurWatchingSite(JSONObject jsonObject) {
        try {
            if (jsonObject.has("_site_id") && !jsonObject.isNull("_site_id")){
                long jsonId = Long.parseLong(jsonObject.getString("_site_id"));
                if (LemurWatchingSite.getById(jsonId) == null){
                    this.id = jsonId;
                }
                if (jsonObject.has("_title") && !jsonObject.isNull("_title")){
                    this.title = jsonObject.getString("_title");
                }else {
                    this.title = "";
                }
                if (jsonObject.has("_body") && !jsonObject.isNull("_body")){
                    this.body = jsonObject.getString("_body");
                }else {
                    this.body = "";
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static LemurWatchingSite getById(long id) {
        Realm realm = Realm.getDefaultInstance();
        return  realm.where(LemurWatchingSite.class).equalTo("id",id).findFirst();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.title);
        dest.writeString(this.body);
    }

    protected LemurWatchingSite(Parcel in) {
        this.id = in.readLong();
        this.title = in.readString();
        this.body = in.readString();
    }

    public static final Creator<LemurWatchingSite> CREATOR = new Creator<LemurWatchingSite>() {
        @Override
        public LemurWatchingSite createFromParcel(Parcel source) {
            return new LemurWatchingSite(source);
        }

        @Override
        public LemurWatchingSite[] newArray(int size) {
            return new LemurWatchingSite[size];
        }
    };

    public static void initData(String data) {
        if (!GenericTools.isNullOrEmpty(data)){
            try {
                JSONObject jsonObject = new JSONObject(data);
                final JSONArray jsonArray = jsonObject.getJSONArray("lemurs_watching_sites");

                Realm realm = Realm.getDefaultInstance();

                for (int i = 0; i < jsonArray.length(); i++)
                {
                    try {
                        realm.beginTransaction();
                        LemurWatchingSite lemurWatchingSite = new LemurWatchingSite(jsonArray.getJSONObject(i));
                        realm.copyToRealmOrUpdate(lemurWatchingSite);
                        realm.commitTransaction();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Log.d(LemurWatchingSite.class.getName(), "No data json ....");
        }
    }

    public LemurWatchingSite() {
    }

    public static ArrayList<LemurWatchingSite> getAll() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<LemurWatchingSite> all = realm.where(LemurWatchingSite.class).findAll();
        return new ArrayList<>(all);
    }
}
