package com.lemursofmadagascar.lom.model.database;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.lemursofmadagascar.lom.tools.GenericTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

public class Author extends RealmObject implements Parcelable {

    @PrimaryKey
    public long id;
    private String name;
    private String photo;
    private String details;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        if (this.photo != null && !this.photo.isEmpty()){
            return String.format("assets://photos/%s", this.photo);
        }else {
            return photo;
        }

    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Author() {
    }

    public Author(JSONObject jsonObject) {

        try {
            if (jsonObject.has("_id") && !jsonObject.isNull("_id")){
                long jsonId = Long.parseLong(jsonObject.getString("_id"));
                if (Author.getById(jsonId) == null){
                    this.id = jsonId;
                }
                if (jsonObject.has("_name") && !jsonObject.isNull("_name")){
                    this.name = jsonObject.getString("_name");
                }else {
                    this.name = "";
                }
                if (jsonObject.has("_details") && !jsonObject.isNull("_details")){
                    this.details = jsonObject.getString("_details");
                }else {
                    this.details = "";
                }
                if (jsonObject.has("_photo") && !jsonObject.isNull("_photo")){
                    this.photo = jsonObject.getString("_photo");
                }else {
                    this.photo = "";
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.photo);
        dest.writeString(this.details);
    }

    protected Author(Parcel in) {
        this.name = in.readString();
        this.photo = in.readString();
        this.details = in.readString();
    }

    public static final Creator<Author> CREATOR = new Creator<Author>() {
        @Override
        public Author createFromParcel(Parcel source) {
            return new Author(source);
        }

        @Override
        public Author[] newArray(int size) {
            return new Author[size];
        }
    };

    public static Author getById(long id){
        Realm realm = Realm.getDefaultInstance();
        return  realm.where(Author.class).equalTo("id",id).findFirst();
    }

    public static void initData(String data) {

        if (!GenericTools.isNullOrEmpty(data)){
            try {
                JSONObject jsonObject = new JSONObject(data);
                final JSONArray jsonArray = jsonObject.getJSONArray("authors");

                Realm realm = Realm.getDefaultInstance();

                for (int i = 0; i < jsonArray.length(); i++)
                {
                    try {
                        realm.beginTransaction();
                        Author author = new Author(jsonArray.getJSONObject(i));
                        realm.copyToRealmOrUpdate(author);
                        realm.commitTransaction();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Log.d(Author.class.getName(), "No data json ....");
        }
    }

    public static ArrayList<Author> getAll() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Author> all = realm.where(Author.class).findAll();
        return new ArrayList<>(all);
    }
}
