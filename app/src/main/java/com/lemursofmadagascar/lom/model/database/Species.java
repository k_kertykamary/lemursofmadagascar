package com.lemursofmadagascar.lom.model.database;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.lemursofmadagascar.lom.tools.GenericTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;


public class Species extends RealmObject implements Parcelable {

    @PrimaryKey
    private long id;

    private long familyId;
    private long profilePhotographId;
    private String identification;
    private String naturalHistory;
    private String geographicRange;
    private String title;
    private String english;
    private String otherEnglish;
    private String french;
    private String german;
    private String malagasy;
    private String map;
    private String whereToSeeIt;
    private String conservationStatus;
    private String speciePhotograph;
    private String favorite;

    public Species(JSONObject jsonObject) {
        try {
            if (jsonObject.has("_species_id") && !jsonObject.isNull("_species_id")){
                long jsonId = Long.parseLong(jsonObject.getString("_species_id"));
                if (Species.getById(jsonId) == null){
                    this.id = jsonId;
                }
                if (jsonObject.has("_profile_photograph_id") && !jsonObject.isNull("_profile_photograph_id")){
                    this.profilePhotographId = Long.parseLong(jsonObject.getString("_profile_photograph_id"));
                }else {
                    this.profilePhotographId = 0;
                }
                if (jsonObject.has("_family_id") && !jsonObject.isNull("_family_id")){
                    this.familyId = Long.parseLong(jsonObject.getString("_family_id"));
                }else {
                    this.familyId = 0;
                }
                if (jsonObject.has("_title") && !jsonObject.isNull("_title")){
                    this.title = jsonObject.getString("_title");
                }else {
                    this.title = "";
                }
                if (jsonObject.has("_english") && !jsonObject.isNull("_english")){
                    this.english = jsonObject.getString("_english");
                }else {
                    this.english = "";
                }
                if (jsonObject.has("_other_english") && !jsonObject.isNull("_other_english")){
                    this.otherEnglish = jsonObject.getString("_other_english");
                }else {
                    this.otherEnglish = "";
                }
                if (jsonObject.has("_french") && !jsonObject.isNull("_french")){
                    this.french = jsonObject.getString("_french");
                }else {
                    this.french = "";
                }
                if (jsonObject.has("_german") && !jsonObject.isNull("_german")){
                    this.german = jsonObject.getString("_german");
                }else {
                    this.german = "";
                }
                if (jsonObject.has("_malagasy") && !jsonObject.isNull("_malagasy")){
                    this.malagasy = jsonObject.getString("_malagasy");
                }else {
                    this.malagasy = "";
                }
                if (jsonObject.has("_identification") && !jsonObject.isNull("_identification")){
                    this.identification = jsonObject.getString("_identification");
                }else {
                    this.identification = "";
                }
                if (jsonObject.has("_natural_history") && !jsonObject.isNull("_natural_history")){
                    this.naturalHistory = jsonObject.getString("_natural_history");
                }else {
                    this.naturalHistory = "";
                }
                if (jsonObject.has("_geographic_range") && !jsonObject.isNull("_geographic_range")){
                    this.geographicRange = jsonObject.getString("_geographic_range");
                }else {
                    this.geographicRange = "";
                }
                if (jsonObject.has("_conservation_status") && !jsonObject.isNull("_conservation_status")){
                    this.conservationStatus = jsonObject.getString("_conservation_status");
                }else {
                    this.conservationStatus = "";
                }
                if (jsonObject.has("_where_to_see_it") && !jsonObject.isNull("_where_to_see_it")){
                    this.whereToSeeIt = jsonObject.getString("_where_to_see_it");
                }else {
                    this.whereToSeeIt = "";
                }
                if (jsonObject.has("_map") && !jsonObject.isNull("_map")){
                    this.map = jsonObject.getString("_map");
                }else {
                    this.map = "";
                }
                if (jsonObject.has("_specie_photograph") && !jsonObject.isNull("_specie_photograph")){
                    this.speciePhotograph = jsonObject.getString("_specie_photograph");
                }else {
                    this.speciePhotograph = "";
                }
                if (jsonObject.has("_favorite") && !jsonObject.isNull("_favorite")){
                    this.favorite = jsonObject.getString("_favorite");
                }else {
                    this.favorite = "";
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static Species getById(long id) {
        Realm realm = Realm.getDefaultInstance();
        return  realm.where(Species.class).equalTo("id",id).findFirst();
    }

    public static ArrayList<Species> getAll() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Species> all = realm.where(Species.class).findAll();
        return new ArrayList<>(all);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getFamilyId() {
        return familyId;
    }

    public void setFamilyId(long familyId) {
        this.familyId = familyId;
    }

    public long getProfilePhotographId() {
        return profilePhotographId;
    }

    public void setProfilePhotographId(long profilePhotographId) {
        this.profilePhotographId = profilePhotographId;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getNaturalHistory() {
        return naturalHistory;
    }

    public void setNaturalHistory(String naturalHistory) {
        this.naturalHistory = naturalHistory;
    }

    public String getGeographicRange() {
        return geographicRange;
    }

    public void setGeographicRange(String geographicRange) {
        this.geographicRange = geographicRange;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getOtherEnglish() {
        return otherEnglish;
    }

    public void setOtherEnglish(String otherEnglish) {
        this.otherEnglish = otherEnglish;
    }

    public String getFrench() {
        return french;
    }

    public void setFrench(String french) {
        this.french = french;
    }

    public String getGerman() {
        return german;
    }

    public void setGerman(String german) {
        this.german = german;
    }

    public String getMalagasy() {
        return malagasy;
    }

    public void setMalagasy(String malagasy) {
        this.malagasy = malagasy;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public String getWhereToSeeIt() {
        return whereToSeeIt;
    }

    public void setWhereToSeeIt(String whereToSeeIt) {
        this.whereToSeeIt = whereToSeeIt;
    }

    public String getConservationStatus() {
        return conservationStatus;
    }

    public void setConservationStatus(String conservationStatus) {
        this.conservationStatus = conservationStatus;
    }

    public String getSpeciePhotograph() {
        return speciePhotograph;
    }

    public void setSpeciePhotograph(String speciePhotograph) {
        this.speciePhotograph = speciePhotograph;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeLong(this.familyId);
        dest.writeLong(this.profilePhotographId);
        dest.writeString(this.identification);
        dest.writeString(this.naturalHistory);
        dest.writeString(this.geographicRange);
        dest.writeString(this.title);
        dest.writeString(this.english);
        dest.writeString(this.otherEnglish);
        dest.writeString(this.french);
        dest.writeString(this.german);
        dest.writeString(this.malagasy);
        dest.writeString(this.map);
        dest.writeString(this.whereToSeeIt);
        dest.writeString(this.conservationStatus);
        dest.writeString(this.speciePhotograph);
        dest.writeString(this.favorite);
    }

    protected Species(Parcel in) {
        this.id = in.readLong();
        this.familyId = in.readLong();
        this.profilePhotographId = in.readLong();
        this.identification = in.readString();
        this.naturalHistory = in.readString();
        this.geographicRange = in.readString();
        this.title = in.readString();
        this.english = in.readString();
        this.otherEnglish = in.readString();
        this.french = in.readString();
        this.german = in.readString();
        this.malagasy = in.readString();
        this.map = in.readString();
        this.whereToSeeIt = in.readString();
        this.conservationStatus = in.readString();
        this.speciePhotograph = in.readString();
        this.favorite = in.readString();
    }

    public static final Creator<Species> CREATOR = new Creator<Species>() {
        @Override
        public Species createFromParcel(Parcel source) {
            return new Species(source);
        }

        @Override
        public Species[] newArray(int size) {
            return new Species[size];
        }
    };


    public static void initData(String data) {
        if (!GenericTools.isNullOrEmpty(data)){
            try {
                JSONObject jsonObject = new JSONObject(data);
                final JSONArray jsonArray = jsonObject.getJSONArray("species");

                Realm realm = Realm.getDefaultInstance();
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    try {
                        realm.beginTransaction();
                        Species species = new Species(jsonArray.getJSONObject(i));
                        realm.copyToRealmOrUpdate(species);
                        realm.commitTransaction();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Log.d(Species.class.getName(), "No data json ....");
        }
    }

    public Species() {
    }

    public Photograph getProfilePhotograph(){
        Realm realm = Realm.getDefaultInstance();
        return realm.where(Photograph.class).equalTo("id", this.profilePhotographId).findFirst();
    }
}
