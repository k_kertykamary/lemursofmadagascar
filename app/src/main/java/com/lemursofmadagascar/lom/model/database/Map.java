package com.lemursofmadagascar.lom.model.database;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.lemursofmadagascar.lom.tools.GenericTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Map extends RealmObject implements Parcelable {

    @PrimaryKey
    private long id;

    private String fileName;

    public Map(JSONObject jsonObject) {
        try {
            if (jsonObject.has("_nid") && !jsonObject.isNull("_nid")){
                long jsonId = Long.parseLong(jsonObject.getString("_nid"));
                if (Map.getById(jsonId) == null){
                    this.id = jsonId;
                }
                if (jsonObject.has("_file_name") && !jsonObject.isNull("_file_name")){
                    this.fileName = jsonObject.getString("_file_name");
                }else {
                    this.fileName = "";
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static Map getById(long id) {
        Realm realm = Realm.getDefaultInstance();
        return  realm.where(Map.class).equalTo("id",id).findFirst();
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.fileName);
    }

    protected Map(Parcel in) {
        this.id = in.readLong();
        this.fileName = in.readString();
    }

    public static final Creator<Map> CREATOR = new Creator<Map>() {
        @Override
        public Map createFromParcel(Parcel source) {
            return new Map(source);
        }

        @Override
        public Map[] newArray(int size) {
            return new Map[size];
        }
    };

    public static void initData(String data) {
        if (!GenericTools.isNullOrEmpty(data)){
            try {
                JSONObject jsonObject = new JSONObject(data);
                final JSONArray jsonArray = jsonObject.getJSONArray("maps");

                Realm realm = Realm.getDefaultInstance();

                for (int i = 0; i < jsonArray.length(); i++)
                {
                    try {
                        realm.beginTransaction();
                        Map map = new Map(jsonArray.getJSONObject(i));
                        realm.copyToRealmOrUpdate(map);
                        realm.commitTransaction();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Log.d(Map.class.getName(), "No data json ....");
        }
    }

    public Map() {
    }
}
