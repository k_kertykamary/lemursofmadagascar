package com.lemursofmadagascar.lom.model.database;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.lemursofmadagascar.lom.tools.GenericTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

public class Family extends RealmObject implements Parcelable {

    @PrimaryKey
    private long id;

    private String familyDescription;
    private String illustration;
    private String family;

    public Family(JSONObject jsonObject) {
        try {
            if (jsonObject.has("_nid") && !jsonObject.isNull("_nid")){
                long jsonId = Long.parseLong(jsonObject.getString("_nid"));
                if (Family.getById(jsonId) == null){
                    this.id = jsonId;
                }
                if (jsonObject.has("_family") && !jsonObject.isNull("_family")){
                    this.family = jsonObject.getString("_family");
                }else {
                    this.family = "";
                }
                if (jsonObject.has("_family_description") && !jsonObject.isNull("_family_description")){
                    this.familyDescription = jsonObject.getString("_family_description");
                }else {
                    this.familyDescription = "";
                }
                if (jsonObject.has("_illustration") && !jsonObject.isNull("_illustration")){
                    this.illustration = jsonObject.getString("_illustration");
                }else {
                    this.illustration = "";
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static Family getById(long id){
        Realm realm = Realm.getDefaultInstance();
        return  realm.where(Family.class).equalTo("id",id).findFirst();
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFamilyDescription() {
        return familyDescription;
    }

    public void setFamilyDescription(String familyDescription) {
        this.familyDescription = familyDescription;
    }

    public String getIllustration() {
        return illustration;
    }

    public void setIllustration(String illustration) {
        this.illustration = illustration;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.familyDescription);
        dest.writeString(this.illustration);
        dest.writeString(this.family);
    }

    protected Family(Parcel in) {
        this.id = in.readLong();
        this.familyDescription = in.readString();
        this.illustration = in.readString();
        this.family = in.readString();
    }

    public static final Creator<Family> CREATOR = new Creator<Family>() {
        @Override
        public Family createFromParcel(Parcel source) {
            return new Family(source);
        }

        @Override
        public Family[] newArray(int size) {
            return new Family[size];
        }
    };

    public static void initData(String data) {
        if (!GenericTools.isNullOrEmpty(data)){
            try {
                JSONObject jsonObject = new JSONObject(data);
                final JSONArray jsonArray = jsonObject.getJSONArray("families");

                Realm realm = Realm.getDefaultInstance();
                for (int i = 0; i < jsonArray.length(); i++)
                {
                    try {
                        realm.beginTransaction();
                        Family family = new Family(jsonArray.getJSONObject(i));
                        realm.copyToRealmOrUpdate(family);
                        realm.commitTransaction();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Log.d(Family.class.getName(), "No data json ....");
        }
    }

    public Family() {
    }

    public static ArrayList<Family> getAll() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Family> all = realm.where(Family.class).findAll();
        return new ArrayList<>(all);
    }
}
