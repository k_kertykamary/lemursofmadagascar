package com.lemursofmadagascar.lom.model.database;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.lemursofmadagascar.lom.tools.GenericTools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;


public class Link extends RealmObject implements Parcelable {

    @PrimaryKey
    private long id;

    private String linkUrl;
    private String linkName;
    private String linkTitle;

    public Link(JSONObject jsonObject) {
        try {
            if (jsonObject.has("_id") && !jsonObject.isNull("_id")){
                long jsonId = Long.parseLong(jsonObject.getString("_id"));
                if (Link.getById(jsonId) == null){
                    this.id = jsonId;
                }
                if (jsonObject.has("_linkname") && !jsonObject.isNull("_linkname")){
                    this.linkName = jsonObject.getString("_linkname");
                }else {
                    this.linkName = "";
                }
                if (jsonObject.has("_linkurl") && !jsonObject.isNull("_linkurl")){
                    this.linkUrl = jsonObject.getString("_linkurl");
                }else {
                    this.linkUrl = "";
                }
                if (jsonObject.has("_linktitle") && !jsonObject.isNull("_linktitle")){
                    this.linkTitle = jsonObject.getString("_linktitle");
                }else {
                    this.linkTitle = "";
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static Link getById(long id) {
        Realm realm = Realm.getDefaultInstance();
        return  realm.where(Link.class).equalTo("id",id).findFirst();
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public String getLinkTitle() {
        return linkTitle;
    }

    public void setLinkTitle(String linkTitle) {
        this.linkTitle = linkTitle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.linkUrl);
        dest.writeString(this.linkName);
        dest.writeString(this.linkTitle);
    }

    protected Link(Parcel in) {
        this.id = in.readLong();
        this.linkUrl = in.readString();
        this.linkName = in.readString();
        this.linkTitle = in.readString();
    }

    public static final Creator<Link> CREATOR = new Creator<Link>() {
        @Override
        public Link createFromParcel(Parcel source) {
            return new Link(source);
        }

        @Override
        public Link[] newArray(int size) {
            return new Link[size];
        }
    };


    public static void initData(String data) {
        if (!GenericTools.isNullOrEmpty(data)){
            try {
                JSONObject jsonObject = new JSONObject(data);
                final JSONArray jsonArray = jsonObject.getJSONArray("links");

                Realm realm = Realm.getDefaultInstance();

                for (int i = 0; i < jsonArray.length(); i++)
                {
                    try {
                        realm.beginTransaction();
                        Link link = new Link(jsonArray.getJSONObject(i));
                        realm.copyToRealmOrUpdate(link);
                        realm.commitTransaction();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Log.d(Link.class.getName(), "No data json ....");
        }
    }

    public Link() {
    }

    public static ArrayList<Link> getAll() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Link> all = realm.where(Link.class).findAll();
        return new ArrayList<>(all);
    }
}
