package com.lemursofmadagascar.lom.tools;

import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Kerty on 06/01/2017.
 */

public class GenericTools {

    public static boolean isNullOrEmpty(String s) {
        return s == null || s.length() == 0;
    }
    public static boolean isNullOrEmpty (EditText textBox) {return !(textBox.getText() != null && textBox.getText().length() > 0);}


}
