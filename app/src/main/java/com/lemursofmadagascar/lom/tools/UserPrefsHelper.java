package com.lemursofmadagascar.lom.tools;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Kerty on 07/01/2017.
 */

public class UserPrefsHelper {

    public static final String IS_THERE_DATA = "need_to_init_data";

    private static UserPrefsHelper ourInstance ;
    private SharedPreferences sharedPreferences;

    public static UserPrefsHelper getInstance(Context context) {

        if (ourInstance == null) {
            ourInstance = new UserPrefsHelper(context);
        }
        return ourInstance;
    }

    private UserPrefsHelper(Context context) {
        sharedPreferences = context.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
    }

    public void saveString(String key,String value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor .putString(key, value);
        prefsEditor.apply();
    }

    public String getString(String key) {
        if (sharedPreferences!= null) {
            return sharedPreferences.getString(key, "");
        }
        return "";
    }

    public void saveInt(String key,int value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor .putInt(key, value);
        prefsEditor.apply();
    }

    public int getInt(String key) {
        if (sharedPreferences!= null) {
            return sharedPreferences.getInt(key, 0);
        }
        return 0;
    }

    public void saveLong(String key,long value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor .putLong(key, value);
        prefsEditor.apply();
    }

    public long getLong(String key) {
        if (sharedPreferences!= null) {
            return sharedPreferences.getLong(key, 0);
        }
        return 0;
    }

    public void saveFloat(String key,float value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor .putFloat(key, value);
        prefsEditor.apply();
    }

    public float getFloat(String key) {
        if (sharedPreferences!= null) {
            return sharedPreferences.getFloat(key, 0);
        }
        return 0;
    }

    public void saveBoolean(String key,boolean value) {
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor .putBoolean(key, value);
        prefsEditor.apply();
    }

    public boolean getBoolean(String key) {
        if (sharedPreferences!= null) {
            return sharedPreferences.getBoolean(key, false);
        }
        return false;
    }

    public void removeValue(String key){
        SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
        prefsEditor .remove(key);
        prefsEditor.apply();
    }

}
