package com.lemursofmadagascar.lom.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.lemursofmadagascar.lom.R;

/**
 * Created by Kerty on 06/01/2017.
 */

public class ProgressDialog extends Dialog {

    public ProgressDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        setContentView(R.layout.dialog_progress);

        WindowManager.LayoutParams layoutParams = this.getWindow().getAttributes();
        layoutParams.gravity= Gravity.TOP;

        this.getWindow().getAttributes().verticalMargin = 0.2f;
        this.getWindow().setAttributes(layoutParams);

        this.setCancelable(false);
    }
}
