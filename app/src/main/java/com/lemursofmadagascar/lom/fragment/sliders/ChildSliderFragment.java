package com.lemursofmadagascar.lom.fragment.sliders;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lemursofmadagascar.lom.R;
import com.lemursofmadagascar.lom.tools.GenericTools;

/**
 * Created by Kerty on 14/01/2017.
 */

public class ChildSliderFragment extends Fragment {

    public static final String KEY_TEXT = "key_text";
    private TextView textViewText;
    private String text;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(KEY_TEXT)){
            this.text = bundle.getString(KEY_TEXT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getFragmentView(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewsByIds(view);
        setViewValuesAndListeners();
    }

    protected int getFragmentView() {
        return R.layout.fragment_child_slider;
    }


    protected void findViewsByIds(View view) {
        textViewText = (TextView)view.findViewById(R.id.textViewText);
    }


    protected void setViewValuesAndListeners() {
        if (textViewText != null) {
            if (!GenericTools.isNullOrEmpty(text)) {
                textViewText.setText(text);
            }
        }
    }


}
