package com.lemursofmadagascar.lom.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lemursofmadagascar.lom.fragment.listener.IBaseFragmentListener;

/**
 * Created by Kerty on 14/01/2017.
 */

public abstract class BaseFragment extends Fragment {

    protected abstract int getFragmentView();
    protected abstract void findViewsByIds(View view);
    protected abstract void setViewValuesAndListeners();
    protected abstract boolean isFragmentWithSearchMenu();

    protected IBaseFragmentListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof IBaseFragmentListener) {
            listener = (IBaseFragmentListener) getActivity();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getFragmentView(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        findViewsByIds(view);
        setViewValuesAndListeners();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (this.listener != null){
            this.listener.hideSearchMenu(isFragmentWithSearchMenu());
        }

    }

}
