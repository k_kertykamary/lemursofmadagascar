package com.lemursofmadagascar.lom.fragment;

import android.os.Bundle;
import android.widget.ListAdapter;

import com.lemursofmadagascar.lom.activity.AuthorDetailsActivity;
import com.lemursofmadagascar.lom.activity.MainActivity;
import com.lemursofmadagascar.lom.adapter.AuthorAdapter;
import com.lemursofmadagascar.lom.model.database.Author;

import java.util.ArrayList;

/**
 * Created by Kerty on 14/01/2017.
 */

public class AboutTheAuthorsFragment extends BaseListFragment {

    private ArrayList<Author> authors;

    @Override
    protected ListAdapter getAdapter() {
        this.authors = Author.getAll();
        if (this.authors != null && !this.authors.isEmpty()){
            return new AuthorAdapter(getContext(), this.authors);
        }else {
            return null;
        }
    }

    @Override
    protected void onItemClick(int position) {
        Author author = this.authors.get(position);
        MainActivity activity = (MainActivity) getActivity();
        Bundle params = new Bundle();
        params.putParcelable(AuthorDetailsActivity.BUNDLE_KEY_AUTHOR, author);
        activity.launchActivity(AuthorDetailsActivity.class, params, false);
    }
}
