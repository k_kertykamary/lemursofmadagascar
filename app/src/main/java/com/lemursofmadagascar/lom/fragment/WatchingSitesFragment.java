package com.lemursofmadagascar.lom.fragment;

import android.widget.ListAdapter;

import com.lemursofmadagascar.lom.adapter.WatchingSiteAdapter;
import com.lemursofmadagascar.lom.model.database.LemurWatchingSite;

import java.util.ArrayList;

/**
 * Created by Kerty on 14/01/2017.
 */

public class WatchingSitesFragment extends BaseListFragment {

    private ArrayList<LemurWatchingSite> lemurWatchingSites;

    @Override
    protected ListAdapter getAdapter() {
        this.lemurWatchingSites = LemurWatchingSite.getAll();
        if (this.lemurWatchingSites != null && !this.lemurWatchingSites.isEmpty()){
            return new WatchingSiteAdapter(getContext(), this.lemurWatchingSites);
        }else {
            return null;
        }
    }

    @Override
    protected void onItemClick(int position) {

    }

    @Override
    protected boolean isFragmentWithSearchMenu() {
        return true;
    }
}
