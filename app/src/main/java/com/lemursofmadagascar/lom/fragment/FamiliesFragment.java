package com.lemursofmadagascar.lom.fragment;

import android.view.View;
import android.widget.ListAdapter;

import com.lemursofmadagascar.lom.adapter.AuthorAdapter;
import com.lemursofmadagascar.lom.adapter.FamilyAdapter;
import com.lemursofmadagascar.lom.model.database.Author;
import com.lemursofmadagascar.lom.model.database.Family;

import java.util.ArrayList;

/**
 * Created by Kerty on 14/01/2017.
 */

public class FamiliesFragment extends BaseListFragment {

    private ArrayList<Family> families;

    @Override
    protected ListAdapter getAdapter() {
        this.families = Family.getAll();
        if (this.families != null && !this.families.isEmpty()){
            return new FamilyAdapter(getContext(), this.families);
        }else {
            return null;
        }
    }

    @Override
    protected void onItemClick(int position) {

    }
}
