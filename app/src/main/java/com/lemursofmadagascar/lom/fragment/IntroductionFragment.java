package com.lemursofmadagascar.lom.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.lemursofmadagascar.lom.fragment.sliders.BaseSliderFragment;
import com.lemursofmadagascar.lom.fragment.sliders.ChildSliderFragment;
import com.lemursofmadagascar.lom.model.database.Menu;
import com.lemursofmadagascar.lom.tools.GenericTools;

import java.util.ArrayList;

/**
 * Created by Kerty on 14/01/2017.
 */

public class IntroductionFragment extends BaseSliderFragment {

    @Override
    protected ArrayList<Fragment> getChildFragments() {

        ArrayList<Fragment> childFragments = new ArrayList<>();
        Menu introductionMenu = Menu.getIntroductionContent();
        if (introductionMenu != null){
            if (!GenericTools.isNullOrEmpty(introductionMenu.getMenuContent())){
                String[] introductionTexts = introductionMenu.getMenuContent().split("###########################################################");
                if (introductionTexts.length > 0){

                    for (String text: introductionTexts){
                        Bundle bundle = new Bundle();
                        bundle.putString(ChildSliderFragment.KEY_TEXT, text);
                        childFragments.add(Fragment.instantiate(getActivity(), ChildSliderFragment.class.getName(),  bundle));
                    }
                }
            }
        }
        return childFragments;
    }
}
