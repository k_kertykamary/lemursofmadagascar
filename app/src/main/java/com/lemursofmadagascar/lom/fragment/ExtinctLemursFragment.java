package com.lemursofmadagascar.lom.fragment;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ListAdapter;

import com.lemursofmadagascar.lom.adapter.ExtinctLemursAdapter;
import com.lemursofmadagascar.lom.model.database.Link;
import com.lemursofmadagascar.lom.tools.GenericTools;

import java.util.ArrayList;

/**
 * Created by Kerty on 14/01/2017.
 */

public class ExtinctLemursFragment extends BaseListFragment {

    private ArrayList<Link> extinctLemurs;

    @Override
    protected ListAdapter getAdapter() {
        this.extinctLemurs = Link.getAll();
        if (this.extinctLemurs != null && !this.extinctLemurs.isEmpty()){
            return new ExtinctLemursAdapter(getContext(), this.extinctLemurs);
        }else {
            return null;
        }
    }

    @Override
    protected void onItemClick(int position) {
        Link link = this.extinctLemurs.get(position);
        if (link != null && !GenericTools.isNullOrEmpty(link.getLinkUrl())) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link.getLinkUrl()));
            startActivity(browserIntent);
        }
    }
}
