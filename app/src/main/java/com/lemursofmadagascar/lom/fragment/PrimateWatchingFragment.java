package com.lemursofmadagascar.lom.fragment;

import android.view.View;

/**
 * Created by Kerty on 14/01/2017.
 */

public class PrimateWatchingFragment extends BaseFragment {
    @Override
    protected int getFragmentView() {
        return 0;
    }

    @Override
    protected void findViewsByIds(View view) {

    }

    @Override
    protected void setViewValuesAndListeners() {

    }

    @Override
    protected boolean isFragmentWithSearchMenu() {
        return true;
    }
}
