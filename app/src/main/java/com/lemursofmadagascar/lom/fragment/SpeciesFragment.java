package com.lemursofmadagascar.lom.fragment;

import android.view.View;
import android.widget.GridView;

import com.lemursofmadagascar.lom.R;
import com.lemursofmadagascar.lom.adapter.SpeciesAdapter;
import com.lemursofmadagascar.lom.adapter.WatchingSiteAdapter;
import com.lemursofmadagascar.lom.model.database.LemurWatchingSite;
import com.lemursofmadagascar.lom.model.database.Species;

import java.util.ArrayList;

/**
 * Created by Kerty on 14/01/2017.
 */

public class SpeciesFragment extends BaseFragment {

    private GridView gridViewSpecies;
    private ArrayList<Species> speciesArrayList;
    private SpeciesAdapter adapter;

    @Override
    protected int getFragmentView() {
        return R.layout.fragment_species;
    }

    @Override
    protected void findViewsByIds(View view) {
        this.gridViewSpecies = (GridView) view.findViewById(R.id.gridViewSpecies);
    }

    @Override
    protected void setViewValuesAndListeners() {

        this.speciesArrayList = Species.getAll();
        if (this.speciesArrayList != null && !this.speciesArrayList.isEmpty()){
            this.adapter = new SpeciesAdapter(this.getContext(), this.speciesArrayList);
            this.gridViewSpecies.setAdapter(this.adapter);
        }
    }

    @Override
    protected boolean isFragmentWithSearchMenu() {
        return true;
    }
}
