package com.lemursofmadagascar.lom.fragment;

import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.lemursofmadagascar.lom.R;

import java.util.ArrayList;

import io.realm.RealmObject;

/**
 * Created by Kerty on 15/01/2017.
 */

public abstract class BaseListFragment extends BaseFragment {

    protected ListView listView;
    protected abstract ListAdapter getAdapter();
    protected abstract void onItemClick(int position);

    @Override
    protected int getFragmentView() {
        return R.layout.fragment_base_list;
    }

    @Override
    protected void findViewsByIds(View view) {
        listView = (ListView) view.findViewById(R.id.baseListView);
    }

    @Override
    protected void setViewValuesAndListeners() {
        ListAdapter adapter = this.getAdapter();

        if (adapter != null) {
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    BaseListFragment.this.onItemClick(position);
                }
            });
        }
    }

    @Override
    protected boolean isFragmentWithSearchMenu() {
        return false;
    }
}
