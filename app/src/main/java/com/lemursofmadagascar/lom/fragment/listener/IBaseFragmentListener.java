package com.lemursofmadagascar.lom.fragment.listener;

/**
 * Created by Kerty on 14/01/2017.
 */

public interface IBaseFragmentListener {
    void hideSearchMenu(boolean hideOrNot);
}
