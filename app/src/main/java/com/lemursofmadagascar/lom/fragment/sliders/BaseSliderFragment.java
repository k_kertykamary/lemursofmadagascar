package com.lemursofmadagascar.lom.fragment.sliders;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.lemursofmadagascar.lom.R;
import com.lemursofmadagascar.lom.adapter.BasePagerAdapter;
import com.lemursofmadagascar.lom.fragment.BaseFragment;

import java.util.ArrayList;

/**
 * Created by Kerty on 14/01/2017.
 */

public abstract class BaseSliderFragment extends BaseFragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;

    protected abstract ArrayList<Fragment> getChildFragments();

    @Override
    protected int getFragmentView() {
        return R.layout.fragment_base_slider;
    }

    @Override
    protected void findViewsByIds(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabDots);
    }

    @Override
    protected void setViewValuesAndListeners() {
        if (viewPager != null) {
            BasePagerAdapter basePagerAdapter = new BasePagerAdapter(super.getChildFragmentManager(), getChildFragments());
            viewPager.setAdapter(basePagerAdapter);

            tabLayout.setupWithViewPager(viewPager, true);
        }
    }

    @Override
    protected boolean isFragmentWithSearchMenu() {
        return false;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
