package com.lemursofmadagascar.lom;

import android.app.Application;
import android.graphics.Bitmap;

import com.lemursofmadagascar.lom.model.database.Author;
import com.lemursofmadagascar.lom.model.database.Family;
import com.lemursofmadagascar.lom.model.database.Illustration;
import com.lemursofmadagascar.lom.model.database.LemurWatchingSite;
import com.lemursofmadagascar.lom.model.database.Link;
import com.lemursofmadagascar.lom.model.database.Map;
import com.lemursofmadagascar.lom.model.database.Menu;
import com.lemursofmadagascar.lom.model.database.Photograph;
import com.lemursofmadagascar.lom.model.database.Species;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import java.io.IOException;
import java.io.InputStream;

import io.realm.Realm;

/**
 * Created by Kerty on 07/01/2017.
 */

@ReportsCrashes( // will not be used
        mailTo = "k.kertykamary@gmail.com",
        customReportContent = { ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME, ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL, ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT },
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast_text)

public class LOMApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ACRA.init(this);
        initImageLoader();

        /** Initialisation et configuration de la base de données locales */
        Realm.init(this);
        initLocalData();
    }


    /**
     * Init the image loader
     */
    private void initImageLoader() {
        DisplayImageOptions defaultDisplayOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .displayer(new RoundedBitmapDisplayer(5))
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration configImageLoader = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .memoryCacheSize(2 * 4096 * 4096)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .discCacheFileNameGenerator(new Md5FileNameGenerator())
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .defaultDisplayImageOptions(defaultDisplayOptions)
                .build();
        ImageLoader.getInstance().init(configImageLoader);
    }

    /**
     * Init the database
     */
    private void initLocalData() {
        Author.initData(loadJSONFromAsset("authors.json"));
        Family.initData(loadJSONFromAsset("families.json"));
        Illustration.initData(loadJSONFromAsset("illustrations.json"));
        LemurWatchingSite.initData(loadJSONFromAsset("lemurs_watching_sites.json"));
        Link.initData(loadJSONFromAsset("links.json"));
        Map.initData(loadJSONFromAsset("maps.json"));
        Menu.initData(loadJSONFromAsset("menu.json"));
        Photograph.initData(loadJSONFromAsset("photographs.json"));
        Species.initData(loadJSONFromAsset("species.json"));
    }

    private String loadJSONFromAsset(String jsonName) {
        String json = null;
        try {
            InputStream is = getAssets().open(jsonName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
