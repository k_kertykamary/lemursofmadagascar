package com.lemursofmadagascar.lom.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lemursofmadagascar.lom.R;
import com.lemursofmadagascar.lom.model.database.Author;
import com.lemursofmadagascar.lom.tools.GenericTools;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by dev on 17/06/2017.
 */

public class AuthorAdapter extends BaseListAdapter {


    public AuthorAdapter(Context context, ArrayList<Author> data) {
        super(context, data);
    }

    @Override
    protected View getAbstractView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_author, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews((Author)getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    private void initializeViews(Author author, ViewHolder holder) {
        if (author != null){

            if (!GenericTools.isNullOrEmpty(author.getName())){
                holder.textViewAuthorName.setText(author.getName());
            }else{
                holder.textViewAuthorName.setText(R.string.not_defined);
            }

            if (!GenericTools.isNullOrEmpty(author.getDetails())){
                holder.textViewContent.setText(author.getDetails());
            }else{
                holder.textViewContent.setText(R.string.not_defined);
            }

            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(author.getPhoto(), holder.imageViewAuthor);

        }else {
            holder.imageViewAuthor.setVisibility(View.INVISIBLE);
            holder.textViewAuthorName.setVisibility(View.INVISIBLE);
            holder.textViewContent.setVisibility(View.INVISIBLE);
        }
    }

    protected class ViewHolder {
        private ImageView imageViewAuthor;
        private TextView textViewAuthorName;
        private TextView textViewContent;

        public ViewHolder(View view) {
            imageViewAuthor = (ImageView) view.findViewById(R.id.imageViewAuthor);
            textViewAuthorName = (TextView) view.findViewById(R.id.textViewAuthorName);
            textViewContent = (TextView) view.findViewById(R.id.textViewContent);
        }
    }
}
