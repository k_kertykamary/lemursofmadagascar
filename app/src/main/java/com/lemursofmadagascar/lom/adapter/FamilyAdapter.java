package com.lemursofmadagascar.lom.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lemursofmadagascar.lom.R;
import com.lemursofmadagascar.lom.model.database.Family;
import com.lemursofmadagascar.lom.tools.GenericTools;

import java.util.ArrayList;

/**
 * Created by dev on 17/06/2017.
 */

public class FamilyAdapter extends BaseListAdapter {

    public FamilyAdapter(Context context, ArrayList<Family> data) {
        super(context, data);
    }

    @Override
    protected View getAbstractView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_family, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews((Family)getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    private void initializeViews(Family family, ViewHolder holder) {

        if (family != null){

            if (!GenericTools.isNullOrEmpty(family.getFamily())){
                holder.textViewFamilyName.setText(family.getFamily());
            }else {
                holder.textViewFamilyName.setText(R.string.not_defined);
            }

            if (!GenericTools.isNullOrEmpty(family.getFamilyDescription())){
                holder.textViewFamilyDetails.setText(family.getFamilyDescription());
            }else {
                holder.textViewFamilyDetails.setText(R.string.not_defined);
            }


        }else{
            holder.textViewFamilyName.setVisibility(View.INVISIBLE);
            holder.textViewFamilyDetails.setVisibility(View.INVISIBLE);
        }
    }

    protected class ViewHolder {
        private TextView textViewFamilyName;
        private TextView textViewFamilyDetails;

        public ViewHolder(View view) {
            textViewFamilyName = (TextView) view.findViewById(R.id.textViewFamilyName);
            textViewFamilyDetails = (TextView) view.findViewById(R.id.textViewFamilyDetails);
        }
    }
}
