package com.lemursofmadagascar.lom.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lemursofmadagascar.lom.R;
import com.lemursofmadagascar.lom.model.database.Photograph;
import com.lemursofmadagascar.lom.model.database.Species;
import com.lemursofmadagascar.lom.tools.GenericTools;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by dev on 17/06/2017.
 */

public class SpeciesAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Species> speciesArrayList;

    public SpeciesAdapter(Context context, ArrayList<Species> speciesArrayList)
    {
        this.context = context;
        this.speciesArrayList = speciesArrayList;
    }


    @Override
    public int getCount() {
        return this.speciesArrayList.size();
    }

    @Override
    public Species getItem(int position) {
        return this.speciesArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {

            gridView = new View(context);

            gridView = inflater.inflate(R.layout.item_species, null);
            Species species = this.speciesArrayList.get(position);


            TextView textViewSpeciesName = (TextView) gridView.findViewById(R.id.textViewSpeciesName);
            ImageView imageViewSpecies = (ImageView) gridView.findViewById(R.id.imageViewSpecies);

            if (species != null){

                if (!GenericTools.isNullOrEmpty(species.getTitle())){
                    textViewSpeciesName.setText(species.getTitle());
                }else {
                    textViewSpeciesName.setText(R.string.not_defined);
                }

                Photograph photograph = species.getProfilePhotograph();
                if (photograph != null) {
                    if (!GenericTools.isNullOrEmpty(photograph.getPhoto())){
                        ImageLoader imageLoader = ImageLoader.getInstance();
                        Log.d(SpeciesAdapter.class.getCanonicalName(), "Photo : " + photograph.getPhoto());
                        imageLoader.displayImage(photograph.getPhoto(), imageViewSpecies);
                    }
                    else{
                        imageViewSpecies.setVisibility(View.INVISIBLE);
                    }
                }else {
                    imageViewSpecies.setVisibility(View.INVISIBLE);
                }

            }else{
                imageViewSpecies.setVisibility(View.INVISIBLE);
                textViewSpeciesName.setVisibility(View.INVISIBLE);
            }

        } else {
            gridView = (View) convertView;
        }

        return gridView;
    }

}
