package com.lemursofmadagascar.lom.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lemursofmadagascar.lom.R;
import com.lemursofmadagascar.lom.model.database.LemurWatchingSite;
import com.lemursofmadagascar.lom.tools.GenericTools;

import java.util.ArrayList;

/**
 * Created by dev on 17/06/2017.
 */

public class WatchingSiteAdapter extends BaseListAdapter {

    public WatchingSiteAdapter(Context context, ArrayList<LemurWatchingSite> data) {
        super(context, data);
    }

    @Override
    protected View getAbstractView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_watching_sites, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews((LemurWatchingSite)getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    private void initializeViews(LemurWatchingSite lemurWatchingSite, ViewHolder holder) {

        if (lemurWatchingSite != null){

            if (!GenericTools.isNullOrEmpty(lemurWatchingSite.getTitle())){
                holder.textViewWatchingSiteName.setText(lemurWatchingSite.getTitle());
            }else {
                holder.textViewWatchingSiteName.setText(R.string.not_defined);
            }

            if (!GenericTools.isNullOrEmpty(lemurWatchingSite.getBody())){
                holder.textViewWatchingSiteDetails.setText(lemurWatchingSite.getBody());
            }else {
                holder.textViewWatchingSiteDetails.setText(R.string.not_defined);
            }


        }else{
            holder.textViewWatchingSiteName.setVisibility(View.INVISIBLE);
            holder.textViewWatchingSiteDetails.setVisibility(View.INVISIBLE);
        }
    }

    protected class ViewHolder {
        private TextView textViewWatchingSiteName;
        private TextView textViewWatchingSiteDetails;

        public ViewHolder(View view) {
            textViewWatchingSiteName = (TextView) view.findViewById(R.id.textViewWatchingSiteName);
            textViewWatchingSiteDetails = (TextView) view.findViewById(R.id.textViewWatchingSiteDetails);
        }
    }
}
