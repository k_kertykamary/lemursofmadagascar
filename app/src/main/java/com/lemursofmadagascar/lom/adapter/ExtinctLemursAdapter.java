package com.lemursofmadagascar.lom.adapter;

/**
 * Created by dev on 17/06/2017.
 */

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lemursofmadagascar.lom.R;
import com.lemursofmadagascar.lom.model.database.Link;
import com.lemursofmadagascar.lom.tools.GenericTools;

import java.util.ArrayList;

public class ExtinctLemursAdapter extends BaseListAdapter {

    public ExtinctLemursAdapter(Context context, ArrayList<Link> data) {
        super(context, data);
    }

    @Override
    protected View getAbstractView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_extinct_lemurs, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        initializeViews((Link)getItem(position), (ViewHolder) convertView.getTag());
        return convertView;
    }

    private void initializeViews(Link link, ViewHolder holder) {

        if (!GenericTools.isNullOrEmpty(link.getLinkTitle())) {
            holder.textViewTitle.setText(link.getLinkTitle());
        }else{
            holder.textViewTitle.setText(R.string.not_defined);
        }
    }

    protected class ViewHolder {
        private TextView textViewTitle;
        public ViewHolder(View view) {
            textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
        }
    }
}

