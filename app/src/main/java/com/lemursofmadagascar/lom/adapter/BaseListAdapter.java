package com.lemursofmadagascar.lom.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lemursofmadagascar.lom.R;
import com.lemursofmadagascar.lom.model.database.Link;

import java.util.ArrayList;

import io.realm.RealmObject;

/**
 * Created by dev on 10/06/2017.
 */

public abstract class BaseListAdapter<T extends RealmObject> extends BaseAdapter {

    protected Context context;
    protected LayoutInflater layoutInflater;
    protected ArrayList<T> data;

    protected abstract View getAbstractView(int position, View convertView, ViewGroup parent);

    public BaseListAdapter(Context context, ArrayList<T> data) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.data = data;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public T getItem(int position) {
        if (data != null && !data.isEmpty()) {
            return data.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return this.getAbstractView(position, convertView, parent);
    }
}
