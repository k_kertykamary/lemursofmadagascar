package com.lemursofmadagascar.lom.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.lemursofmadagascar.lom.fragment.BaseFragment;

import java.util.ArrayList;

/**
 * Created by Kerty on 14/01/2017.
 */

public class BasePagerAdapter extends FragmentPagerAdapter {

    private ArrayList<Fragment> fragments;

    public BasePagerAdapter(FragmentManager fm, ArrayList<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    @Override
    public int getCount() {
        return this.fragments.size();
    }
}
